package br.com.certibio.facecheckdemoandroid

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import br.com.certibio.facecheckdemoandroid.ui.main.MainFragment
import kotlinx.android.synthetic.main.main_fragment.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"
    }

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        addNavigationSupportToActionBar()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val ret = super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu, menu)
        return ret
    }

    // https://developer.android.com/guide/navigation/navigation-ui#action_bar
    private fun addNavigationSupportToActionBar() {
        navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    // https://developer.android.com/guide/navigation/navigation-ui#action_bar
    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    fun settingsClicked(item: MenuItem) {
        Log.d(TAG, "settings clicked")
        navController.navigate(R.id.action_global_settingsFragment)
    }
}