package br.com.certibio.facecheckdemoandroid.ui.main

import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import br.com.certibio.facecheckdemoandroid.R

class SettingsFragment : PreferenceFragmentCompat() {

    companion object {
        private const val TAG = "SettingsFragment"
    }

    private lateinit var passwordPreferences: EditTextPreference

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        configAppInfos()
        configPasswordPreference()
    }

    private fun configPasswordPreference() {
        findPreference<EditTextPreference>("password")?.run {
            setOnBindEditTextListener { editText ->
                editText.inputType =
                    InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            }

            setOnPreferenceChangeListener { preference, newValue ->
                Log.d(TAG, "Executing setOnPreferenceChangeListener with newValue=$newValue")
                val newString = (newValue as String).trim()
                if (newString.contains(" ")) {
                    false
                } else {
                    text = newString
                    true
                }
            }

            summaryProvider = Preference.SummaryProvider<EditTextPreference> { preference ->
                if (TextUtils.isEmpty(preference.text)) {
                    resources.getString(R.string.preference_not_set)
                } else {
                    "[${preference.text}]"
                }
            }
        }
    }

    private fun configAppInfos() {
        val edit = preferenceManager.sharedPreferences.edit()

        findPreference<EditTextPreference>("appVersion")?.let { versionPreference ->
            activity?.run {
                versionPreference.text = packageManager.getPackageInfo(packageName, 0).versionName
            }
        }
    }

}