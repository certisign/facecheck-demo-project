package br.com.certibio.facecheckdemoandroid.service

import android.app.IntentService
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat


class LoginService : IntentService("LoginService") {

    companion object {
        private const val TAG = "LoginService"

        private val NOTIFY_ID = 1337
        private val FOREGROUND_ID = 1338
        private const val CHANNEL_ID = "LoginService-Channel"
    }

    override fun onHandleIntent(intent: Intent?) {
        Log.d(TAG, "Thread name: ${Thread.currentThread().name}")
        startForeground( FOREGROUND_ID, NotificationCompat.Builder(applicationContext, CHANNEL_ID).build())
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show()
        return super.onStartCommand(intent, flags, startId)
    }
}
