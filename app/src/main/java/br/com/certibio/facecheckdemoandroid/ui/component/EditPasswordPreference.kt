package br.com.certibio.facecheckdemoandroid.ui.component

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import androidx.preference.EditTextPreference

/**
 * Implementation to remove trailing blanks from the password.
 *
 * See https://stackoverflow.com/questions/5858790/process-the-value-of-preference-before-save-in-android
 *
 * Looking at androidx.preference.PreferenceInflater, method createItem, it's possible to see that the
 * only constructor invoked it the one with the two arguments implemented in here.
 *
 * @author Valdemar Arantes Neto
 * @since 02/27/2020
 */
class EditPasswordPreference(context: Context?, attrs: AttributeSet? = null) :
    EditTextPreference(context, attrs) {

    companion object {
        private const val TAG = "EditPasswordPreference"
    }

    override fun getText(): String? = super.getText().also {
        Log.d(TAG, "getText invoked. Returning $it")
    }

    /**
     * Here is the point where the trailing blanks are wiped out.
     */
    override fun setText(text: String?) {
        Log.d(TAG, "setText invoked with text = $text")
        text?.trim().let {
            Log.d(TAG, "After trim() setting text=$it")
            super.setText(it)
        }
    }
}